let http = require ("http");
//We use the "require" directive to load Node.js modules
//a module is a software component or part of a program that contains one or more routines
//HTTP is a protocol that allows the fetching of resources such as HTML documents

http.createServer(function (request, response){

//Use writeHead

    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hello World');

}).listen(4000)

console.log('Server Running at localhost:4000');